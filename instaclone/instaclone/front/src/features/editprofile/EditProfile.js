import React, { useEffect, useState } from "react";
import {
  Image,
  Box,
  Form,
  FormField,
  TextInput,
  FileInput,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
} from "grommet";
import * as Icons from "grommet-icons";
import { useHistory } from "react-router";
import fire from "../../fire";
// import "./style.css";

const EditProfile = () => {
  const [user, setUser] = useState({});
  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  useEffect(() => {
    const currentUser = fire.auth().currentUser;
    setUser(currentUser);
  }, []);
  useEffect(() => {
    setName(user.displayName);
    setImage(user.photoURL);
  }, [user.displayName, user.photoURL]);
  function uploadBlob(file) {
    return new Promise((fulfill, reject) => {
      const ref = fire
        .storage()
        .ref()
        .child("image-" + Date.now());

      // [START storage_upload_blob]
      // 'file' comes from the Blob or File API
      ref
        .put(file)
        .then((snapshot) => {
          fulfill(snapshot);
        })
        .catch((err) => reject(err));
      // [END storage_upload_blob]
    });
  }
  const history = useHistory();
  return (
    <Card
      height="large"
      width="medium"
      background="light-1"
      align="center"
      margin="auto"
    >
      <CardHeader pad="medium">
        <p>Edit Profile</p>
        <Button
          onClick={() => {
            fire.auth().signOut();
            history.push("/");
          }}
          label="Signout"
        />
      </CardHeader>
      <CardBody pad="medium">
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            if (!name || !image) return;
            user.updateProfile({
              displayName: name,
              photoURL: image,
            });
            setUser(fire.auth().currentUser);
          }}
        >
          <FormField
            type="text"
            id="new-name"
            class="fadeIn second"
            label="Name"
            name="name"
            component={TextInput}
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />

          <Box
            height="small"
            width="small"
            style={{ margin: "auto", marginTop: "15px" }}
          >
            <Image
              fit="cover"
              src={image}
              style={{ maxWidth: 400, maxHeight: 400 }}
            />
          </Box>
          <FormField
            margin="10px 0 0 0"
            type="file"
            component={FileInput}
            onChange={async (e) => {
              if (!e.target.files.length) return;
              const file = e.target.files[0];
              const snap = await uploadBlob(file);
              const downloadUrl = await snap.ref.getDownloadURL();
              setImage(downloadUrl);
            }}
          />
          <div style={{ marginTop: 10 }}>
            <Button
              label="Save"
              onClick={(e) => {
                e.preventDefault();
                if (!name || !image) return;
                user.updateProfile({
                  displayName: name,
                  photoURL: image,
                });
                setUser(fire.auth().currentUser);
              }}
            />
            <Button label="Cancel" onClick={() => history.goBack()} />
          </div>
        </Form>
      </CardBody>
      <CardFooter
        pad={{ horizontal: "small" }}
        background="light-2"
      ></CardFooter>
    </Card>
  );
};

export default EditProfile;
