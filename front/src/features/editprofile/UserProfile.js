import React, { useEffect, useState } from "react";
import { Edit, Favorite, Trash } from "grommet-icons";
import {
  Row,
  Col,
  Card,
  Button,
  Badge,
  Container,
  Tabs,
  Tab,
} from "react-bootstrap";
import { Link, useHistory, useLocation, useParams } from "react-router-dom";
import fire from "../../fire";

import { useDispatch, useSelector } from "react-redux";
import {
  getPostsByUesr,
  getPostsByUserId,
  getUserById,
  getPostsByLikes,
  getCollectionsByUserId,
  getCollectionsByUesr,
  deleteCollection,
  removeFromCollection,
} from "../../services/postsServices";
import { update } from "../posts/postsSlice";
import { Avatar, Box } from "grommet";
import placeholder from "../../placeholder.png";

const UserProfile = ({ user: loggedInUser = {} }) => {
  const currentUser = loggedInUser;
  const [key, setKey] = useState("collections");
  const [likedPosts, setLikedPosts] = useState([]);
  const [collections, setcollections] = useState([]);

  const [otherUser, setOtherUser] = useState(null);
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();
  // const posts = useSelector((state) => state.posts.value);
  useEffect(() => {
    if (params.email) {
      (async () => {
        const fetchdPosts = await getCollectionsByUserId(params.email);
        setcollections(fetchdPosts);
        const foundUser = await getUserById(params.email);
        setOtherUser(foundUser);
        // dispatch(update(fetchdPosts));
      })();
    } else {
      (async () => {
        const fetchdPosts = await getCollectionsByUesr();
        setcollections(fetchdPosts);
        const list = await getPostsByLikes();
        setLikedPosts(list);
        // dispatch(update(fetchdPosts));
      })();
    }
  }, []);
  const user = otherUser || loggedInUser;
  let name,
    handle = "";
  if (user && user.displayName) [name, handle] = user.displayName?.split(",");
  console.log();
  return (
    <Container>
      <Row>
        <h2>User Profile</h2>
      </Row>
      <Row>
        <Col md={5} lg={5}>
          <Row>
            <Col md={2} lg={2}>
              <Avatar src={user.photoURL} />
            </Col>
            <Col md={8} lg={8} as="p" style={{ marginTop: 7, marginRight: 20 }}>
              {name || ""}
              <br />
              {handle && (
                <Link to={`user-profile/${user.email}`}>{"@" + handle}</Link>
              )}
            </Col>
          </Row>
        </Col>
        {/* <Col /> */}
        <Col></Col>
        <Col>
          <Button onClick={() => history.push("/create-collection")}>
            Create New Collection
          </Button>
          <Button
            variant="secondary"
            onClick={() => history.push("/edit-profile")}
            style={{ marginLeft: 7 }}
          >
            Edit Profile
          </Button>
        </Col>{" "}
      </Row>
      <Row style={{ marginTop: 25 }}>
        <Col>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
          >
            <Tab eventKey="collections" title="Collections">
              <Row>
                {collections.map((post) => {
                  return (
                    <Col
                      md={12}
                      lg={12}
                      sm={12}
                      style={{ marginTop: 5, color: "black" }}
                    >
                      <h2>{post.title} </h2>
                      <Box
                        horizontal="medium"
                        vertical="medium"
                        direction="row"
                        justify="space-between"
                      >
                        {post.user === user.email && (
                          <Edit
                            onClick={() =>
                              history.push("/update-collection/" + post.id)
                            }
                            style={{ margin: 20 }}
                          />
                        )}

                        {post.user === user.email && (
                          <Trash
                            onClick={() => {
                              deleteCollection({ collectionId: post.id });
                              window.location.reload();
                            }}
                            style={{ margin: 20 }}
                          />
                          // <Button
                          //   label="Delete"

                          // ></Button>
                        )}
                      </Box>
                      <hr />
                      <div class="row">
                        {post.posts.map((item) => {
                          return (
                            <div className="col-lg-4 col-md-12 mb-4 mb-lg-0">
                              <Trash
                                onClick={async () => {
                                  try {
                                    await removeFromCollection(
                                      post.id,
                                      item.id
                                    );
                                    setcollections(
                                      collections.map((col) =>
                                        col.id === post.id
                                          ? {
                                              ...col,
                                              posts: col.posts.filter(
                                                (p) => p.id !== item.id
                                              ),
                                            }
                                          : col
                                      )
                                    );
                                  } catch (err) {}
                                }}
                                style={{}}
                              />
                              <img
                                src={
                                  item.img ||
                                  placeholder ||
                                  "https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg"
                                }
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt=""
                              />
                            </div>
                          );
                        })}
                      </div>
                      <hr />
                    </Col>
                  );
                })}
              </Row>
            </Tab>
            {!params.email && (
              <Tab eventKey="favorites" title="Favorites">
                <Row style={{ marginTop: 15 }}>
                  {likedPosts.map((post) => {
                    return (
                      <Col
                        md={3}
                        lg={3}
                        sm={12}
                        style={{ marginTop: 5, color: "black" }}
                      >
                        <Card>
                          <Card.Img
                            variant="top"
                            src={post.img || placeholder}
                          />
                          <Card.Body>
                            <Card.Title>{post.user.split("@")[0]}</Card.Title>
                            <Card.Text>
                              <div>
                                {post.content}
                                <div>
                                  {post.tags?.map((tag) => (
                                    <Badge
                                      variant="secondary"
                                      style={{ marginLeft: 5 }}
                                    >
                                      {tag}
                                    </Badge>
                                  ))}
                                </div>
                              </div>
                            </Card.Text>
                            {/* <Button variant="primary">Go somewhere</Button> */}
                          </Card.Body>
                        </Card>
                      </Col>
                    );
                  })}
                </Row>
              </Tab>
            )}
          </Tabs>
        </Col>
      </Row>
    </Container>
  );
};

export default UserProfile;
