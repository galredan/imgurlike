import { Box, FormField, TextInput, Form, Button } from "grommet";
import React, { useEffect, useState } from "react";
import { addComment } from "../../services/postsServices";

const Comment = ({ post, handleCancel }) => {
  const [comment, setComment] = useState("");
  const [loading, setLoading] = useState(false);
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (comment) {
      setLoading(true);
      //   await addComment(comment, post.id);
      //   setComment("");
      //   setLoading(false);
    }
  };
  useEffect(() => {
    (async () => {
      if (loading) {
        await addComment(comment, post.id);
        setLoading(false);
        setComment("");
      }
    })();
  }, [loading, addComment]);
  return (
    <Box direction="column" width="medium" height="medium">
      <Form onSubmit={handleSubmit}>
        <FormField
          component={TextInput}
          value={comment}
          label="Comment"
          placeholder="Enter Comment"
          onChange={(e) => setComment(e.target.value)}
        ></FormField>
        <Button disabled={loading} onClick={handleSubmit} label="Post"></Button>
        <Button onClick={handleCancel} label="Cancel"></Button>
      </Form>
    </Box>
  );
};

export default Comment;
