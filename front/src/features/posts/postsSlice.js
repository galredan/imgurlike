import { createSlice } from "@reduxjs/toolkit";

export const postsSlice = createSlice({
  name: "posts",
  initialState: {
    value: [],
  },
  reducers: {
    update: (state, action) => {
      state.value = action.payload;
    },
    updateSingle: (state, action) => {
      console.log("here it comes to update single", action.payload);
      state.value = state.value.map((item) =>
        item.id === action.payload.id ? action.payload : item
      );
    },
  },
});

export const { update, updateSingle } = postsSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectPosts = (state) => state.posts.value;

export default postsSlice.reducer;
