import React, { useState } from "react";

import { Box, Button, Image, Text, Layer } from "grommet";
import { Edit, Favorite, Trash } from "grommet-icons";

import Card from "../../components/Card";
import CardConcave from "../../components/CardConcave";
import Comment from "./Comment";

import { Scrollbars } from "react-custom-scrollbars";

import {
  addPost,
  getPosts,
  likePost,
  unlikePost,
  deletePost,
  addToCollection,
  getCollectionsByUesr,
} from "../../services/postsServices";
import { useSelector, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { update, selectPosts } from "./postsSlice";
import fire from "../../fire";
import { Badge } from "react-bootstrap";
const Posts = ({ user = {} }) => {
  const posts = useSelector(selectPosts);
  console.log({ user, posts });
  const [collections, setcollections] = useState([]);
  const [selectedCollection, setSelectedCollection] = useState("");
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const [refresh, setrefresh] = React.useState(true);
  const [showComment, setShowComment] = React.useState(null);
  const like = (post) => {
    likePost(post).then(() => setrefresh(true));
  };
  const unlike = (post) => {
    unlikePost(post).then(() => setrefresh(true));
  };

  React.useEffect(() => {
    const fecthPosts = async () => {
      const fetchData = await getPosts();
      const fetchCollections = await getCollectionsByUesr();
      setcollections(fetchCollections);
      dispatch(update(fetchData));
    };
    // if (refresh) {
    //   fecthPosts();
    //   setrefresh(false);
    // }
    fecthPosts();
  }, []);

  return (
    <Box align="center" margin="medium">
      <Card
        round="medium"
        padding="medium"
        justify="center"
        align="center"
        margin="medium"
        width="large"
        fill="vertical"
      >
        {posts && posts.length ? (
          <Button>
            <Link to="/createPost">Create New</Link>
          </Button>
        ) : (
          ""
        )}
        <Scrollbars autoHeight autoHeightMax="100%">
          {posts ? (
            !posts.length ? (
              <Text>
                Posts Not Found OR Error comes in fetching posts.{" "}
                {<Link to="/createPost">Click Here</Link>} to create new post
              </Text>
            ) : (
              posts.map((post, index) => (
                <CardConcave
                  round="small"
                  padding="medium"
                  margin={{ vertical: "small", horizontal: "large" }}
                  alignSelf="center"
                >
                  <Box margin={{ horizontal: "small", top: "small" }}>
                    <Link to={"/user-profile/" + post.user}>
                      <Text size="xsmall" weight="bold">
                        {"@" + post.user.split("@")[0]}
                      </Text>
                    </Link>
                  </Box>
                  <Box
                    horizontal="medium"
                    vertical="medium"
                    direction="row"
                    justify="space-between"
                  >
                    {post.user === user.email && (
                      <Edit
                        onClick={() => history.push("/update-post/" + post.id)}
                        style={{ margin: 20 }}
                      />
                    )}

                    {post.user === user.email && (
                      <Trash
                        onClick={() => {
                          deletePost({ postId: post.id });
                          window.location.href = "/";
                        }}
                        style={{ margin: 20 }}
                      />
                      // <Button
                      //   label="Delete"

                      // ></Button>
                    )}
                  </Box>
                  <Box width="small" direction="column-reverse">
                    <Box width="medium" />
                    <Box width="medium">
                      <Button
                        margin="10px"
                        onClick={() => setShow(post.id)}
                        label="Add to Collection"
                      ></Button>
                    </Box>
                  </Box>
                  <Box
                    margin={{ horizontal: "small", bottom: "small" }}
                    direction="column"
                    justify="between"
                    align="center"
                  >
                    <Box fill="horizontal" gap="small">
                      <Text color="white">{post.content}</Text>
                      <Text color="white">
                        {post.tags
                          ? post.tags.map((tag) => (
                              <Badge
                                variant="secondary"
                                style={{ marginLeft: 15 }}
                              >
                                {tag}
                              </Badge>
                            ))
                          : ""}
                      </Text>
                      <Box fill="horizontal" align="center">
                        {post.img && (
                          <Image
                            src={`${post.img}`}
                            fit="cover"
                            style={{ maxWidth: 400, maxHeight: 400 }}
                          />
                        )}
                      </Box>
                    </Box>

                    <Box
                      direction="row"
                      justify="center"
                      align="center"
                      gap="small"
                    >
                      {post.likes.find(
                        (item) => item === fire.auth().currentUser.email
                      ) ? (
                        <Button
                          icon={<Favorite size="small" color="red" />}
                          onClick={() => unlike(post)}
                        />
                      ) : (
                        <Button
                          icon={<Favorite size="small" />}
                          onClick={() => like(post)}
                        />
                      )}

                      <Text size="small" color="white">
                        {post.likes?.length}
                      </Text>
                      <Text
                        size={"small"}
                        color="white"
                        style={{ cursor: "pointer" }}
                        onClick={() => setShowComment(index)}
                      >
                        Comment {post.comments?.length}
                      </Text>
                    </Box>
                    {showComment === index ? (
                      <Box
                        direction="column"
                        justify="center"
                        align="center"
                        gap="small"
                      >
                        {post.comments.map((comment) => {
                          return (
                            <>
                              <Box
                                margin={{ horizontal: "small", top: "small" }}
                              >
                                <Text color="white" size="xsmall" weight="bold">
                                  {comment.user.split("@")[0]}
                                </Text>
                              </Box>

                              <Box
                                margin={{
                                  horizontal: "small",
                                  bottom: "small",
                                }}
                                direction="column"
                                justify="between"
                                align="start"
                              >
                                <Box fill="horizontal" gap="small">
                                  <Text color="white">{comment.content}</Text>
                                </Box>
                              </Box>
                            </>
                          );
                        })}

                        <Box
                          direction="column"
                          justify="center"
                          align="center"
                          gap="small"
                        >
                          <Comment
                            post={post}
                            handleCancel={() => setShowComment(null)}
                          />
                        </Box>
                      </Box>
                    ) : (
                      ""
                    )}
                  </Box>
                  {show === post.id && (
                    <Layer
                      onEsc={() => setShow(false)}
                      onClickOutside={() => setShow(false)}
                    >
                      <Box
                        width="medium"
                        height="medium"
                        alignContent="center"
                        justify="center"
                      >
                        <Box
                          width="medium"
                          height="medium"
                          alignContent="center"
                          justify="center"
                        >
                          <h2 style={{ color: "black" }}>Select Collection</h2>
                          <form
                            className="form m-4"
                            onSubmit={async (e) => {
                              e.preventDefault();
                              if (selectedCollection) {
                                await addToCollection(
                                  selectedCollection,
                                  post.id
                                );
                                setShow(false);
                              }
                            }}
                          >
                            <select
                              className="form-control mb-4"
                              value={selectedCollection}
                              onChange={(e) =>
                                setSelectedCollection(e.target.value)
                              }
                            >
                              <option value="">Select Collection</option>
                              {collections.map((col) => (
                                <option key={col.id} value={col.id}>
                                  {col.title}
                                </option>
                              ))}
                            </select>
                            <button
                              className="btn btn-primary"
                              onClick={async (e) => {
                                e.preventDefault();
                                if (selectedCollection) {
                                  await addToCollection(
                                    selectedCollection,
                                    post.id
                                  );
                                  setShow(false);
                                }
                              }}
                            >
                              Add
                            </button>
                            <button
                              className="btn btn-secondary ml-2"
                              onClick={() => setShow(false)}
                            >
                              Cancel
                            </button>
                          </form>
                        </Box>
                      </Box>
                    </Layer>
                  )}
                </CardConcave>
              ))
            )
          ) : (
            <Text>Posts Not found</Text>
          )}
        </Scrollbars>
      </Card>
      <Box></Box>
    </Box>
  );
};

export default Posts;
