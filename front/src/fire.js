import firebase from "firebase";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyAU1srtIEvqqjIZBJzC8moQ3-SbvNMvTYU",
  authDomain: "imgurlike.firebaseapp.com",
  projectId: "imgurlike",
  storageBucket: "imgurlike.appspot.com",
  messagingSenderId: "637449985299",
  appId: "1:637449985299:web:d6c34c2aad921f0ba40ee6"
};

try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error("Firebase initialization error", err.stack);
  }
}
const fire = firebase;
export default fire;
