const postsRouter = require("express").Router();
const Post = require("../models/post");
const multer = require("multer");
const { firebaseAdmin } = require("../authenticateToken");
const { v4: uuidv4 } = require("uuid");
let path = require("path");

postsRouter.get("/", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    const posts = await Post.find({});

    req.io.emit("UPDATE", posts);
    return res.json(posts.map((post) => post.toJSON()));
  }
  return res.status(403).send("Not authorized");
});

postsRouter.get("/postsByUser/:email", async (req, res) => {
  const auth = req.currentUser;
  const { email } = req.params;
  if (auth) {
    const posts = await Post.find({ user: email });

    // req.io.emit("UPDATE", posts);
    return res.json(posts.map((post) => post.toJSON()));
  }
  return res.status(403).send("Not authorized");
});

postsRouter.get("/postsByLike", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    const posts = await Post.find({ likes: { $in: [auth.email] } });

    req.io.emit("UPDATE", posts);
    return res.json(posts.map((post) => post.toJSON()));
  }
  return res.status(403).send("Not authorized");
});

postsRouter.get("/userById/:email", async (req, res) => {
  const auth = req.currentUser;
  const { email } = req.params;

  if (auth) {
    if (auth.email === email) return res.json(auth);
    const user = await firebaseAdmin.auth().getUserByEmail(email);
    return res.json(user);
  }
  return res.status(403).send("Not authorized");
});

postsRouter.get("/postsByUser", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    const posts = await Post.find({ user: auth.email });

    req.io.emit("UPDATE", posts);
    return res.json(posts.map((post) => post.toJSON()));
  }
  return res.status(403).send("Not authorized");
});

postsRouter.get("/singlePost/:id", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    let post = await Post.findById(req.params.id);
    if (post.user !== auth.email) return res.status(401).send("Not authorized");
    post = {
      ...post,
      comments: post.comments
        ? await Promise.all(
            post.comments.map(async (comment) => {
              const userWhoCommeted = await firebaseAdmin
                .auth()
                .getUserByEmail(req.currentUser.email);
              return {
                ...comment,
                userName: userWhoCommeted.displayName,
              };
            })
          )
        : [],
    };
    // req.io.emit("SINGLE_POST", post);
    return res.json(post._doc);
  }
  return res.status(403).send("Not authorized");
});

postsRouter.post("/updatePost", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    const post = await Post.findById(req.body.postId);
    post.content = req.body.content;
    post.tags = req.body.tags;
    await post.save();

    // req.io.emit("UPDATE", posts);
    return res.json(post);
  }
  return res.status(403).send("Not authorized");
});

postsRouter.post("/deletePost", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    const post = await Post.findById(req.body.postId);
    if (post) await Post.findByIdAndRemove(req.body.postId);

    // req.io.emit("UPDATE", posts);
    return res.json(post);
  }
  return res.status(403).send("Not authorized");
});

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "images");
  },
  filename: function (req, file, cb) {
    cb(null, uuidv4() + "-" + Date.now() + path.extname(file.originalname));
  },
});

const fileFilter = (req, file, cb) => {
  const allowedFileTypes = ["image/jpeg", "image/jpg", "image/png"];
  if (allowedFileTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

let upload = multer({ storage, fileFilter });

postsRouter.route("/").post(async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    const post = new Post({
      content: req.body.content,
      img: req.body.img,
      tags: req.body.tags,
      likes: [],
      comments: [],
      user: req.currentUser.email,
    });
    const savedPost = post.save();
    const posts = await Post.find({});
    req.io.emit("UPDATE", posts);
    return res.status(201).json(savedPost);
  }
  return res.status(403).send("Not authorized");
});

postsRouter.post("/like", async (req, res) => {
  const auth = req.currentUser;
  if (auth) {
    try {
      const post = await Post.findById(req.body.id);
      const { like } = req.body;
      console.log(req.body);
      post.likes = post.likes
        ? like
          ? post.likes.concat(req.currentUser.email)
          : post.likes.filter((item) => item !== req.currentUser.email)
        : [{ user: req.currentUser.email, content: req.body.content }];
      const saved = post.save();
      let updatedPost = await Post.findById(req.body.id);
      updatedPost = {
        ...updatedPost,
        comments: updatedPost.comments
          ? await Promise.all(
              updatedPost.comments.map(async (comment) => {
                const userWhoCommeted = await firebaseAdmin
                  .auth()
                  .getUserByEmail(req.currentUser.email);
                return {
                  ...comment,
                  userName: userWhoCommeted.displayName,
                };
              })
            )
          : [],
      };
      // req.io.emit("UPDATE", posts);
      req.io.emit("SINGLE_POST", post);
      return res.status(201).json(post);
    } catch (e) {
      console.log(e);
    }
  }
  return res.status(500).send("Error server");
});

postsRouter.post("/comment", async (req, res) => {
  const auth = req.currentUser;
  console.log({ auth });
  if (auth) {
    try {
      const post = await Post.findById(req.body.id);
      console.log(req.body);
      post.comments = post.comments
        ? post.comments.concat({
            user: req.currentUser.email,
            content: req.body.content,
          })
        : [{ user: req.currentUser.email, content: req.body.content }];
      // post.comments = req.body.comments;
      const saved = post.save();
      let updatedPost = await Post.findById(req.body.id);
      updatedPost = {
        ...updatedPost,
        comments: updatedPost.comments
          ? await Promise.all(
              updatedPost.comments.map(async (comment) => {
                const userWhoCommeted = await firebaseAdmin
                  .auth()
                  .getUserByEmail(req.currentUser.email);
                return {
                  ...comment,
                  userName: userWhoCommeted.displayName,
                };
              })
            )
          : [],
      };
      // req.io.emit("UPDATE", posts);
      req.io.emit("SINGLE_POST", post);
      return res.status(201).json(saved);
    } catch (e) {
      console.log(e);
    }
  }
  return res.status(500).send("Error server");
});

module.exports = postsRouter;
