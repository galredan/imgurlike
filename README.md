**Imgur Like - Fullstack Project**

**Setup & Installation :**
<br>
_installer Yarn_
<br>
`cd front`
<br>
`yarn install`
<br>
`cd ../back`
<br>
`yarn install`
<br>
`cd ..`
<br>
`yarn install` (si possible)
<br>
`yarn dev`

**Identifiants des 2 comptes de test :**
<br>
test@test.com pwd001
<br>
test2@test.com pwd002

**Informations sur le projet**

-   fait en MERN (Mongo & Mongoose, Express, React, NodeJS)
-   images stockées sur FireBase, data sur MongoDB Atlas
-   maquettes et pictogrammes réalisés à la main sur Illustrator
-   ressources (User Stories et Maquettes) dans le dossier **Concept - Maquettes**
